/*
	What is a client?
		A client in an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server

	What is a server?

		A server is able to host and deliver resoures requested by a client. In fact, a single server can handle multiple clients.

	What is Node.js?

		Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with Javascript. Because by default, JS was cpnceptualized solely to the front end

	Why is NodeJS popular?

		Performance - NodeJS is one of the most performing environment for creating backend applications with JS

		Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers

		NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application

*/

// console.log("Hello World! Yeah.");


//browser client, client requests from url,


let http = require("http");

//require() is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application

//"http" is a default package that comes with NodeJS. It allows us to use methods that let us create servers.////yesss (inimport natin to create a server) (package that enales us to communicate through http)(http na iniimport-package un)

//http is a module. Modules are packages we imported 
//Modules are objects that contains codes, functions, methods or data

//the http module let us create a server which is able to communicate with a client through the use of hypertext transdfer protocol

//protocol to client-serve communication - http://localhost:4000 - server/application

// console.log(http);

http.createServer(function(request,response){
	//create server is a method from the http module
	/*
	createServer() method is a method from the http module that allows us to handle requests and responses from a client and a server respectively

		.createServer() method takes a function argument which is able to receive 2 objects.

		The request object which contains details of the request from the client
		The response object which contains details of the response from the server!

		The createServer() method ALWAYS receives the request object first before the response


		The response.writeHead() - is a method of the response object. It allows us to add headers to our responses. Headers are additional information about our response.
		we have two arguments in our WH method, the first is the HTTP status code. An  HTTP status is just a numerical code to let the client the know about the status of their request. 200, means OK, 404 means the resources cannot be found
		(number - good bads, yung request)
		content type - one of the most recognizable headers
		it simply pertains to the //data type// of our responses. content type mas makikita natin sa postman
		

		response.end() it ends our response, tapos na wag na balikan, tapos na kung tapoooos na




	*/

	//console.log(request); sobrang dami, pero di naman lahat, its able to send a message/data as a string///aceept lang nya sring


	// console.log(request.url)
	//URLs are strings

	//specific responses to a specific endpoint
	if(request.url === "/"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our first Server! This is from / endpoint");
	}
	/*else if(request.url==="/cami"){
		response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our first Server! This is from cami")
	}
*/
	else if (request.url === "/profile"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hi! I am Cami!!!");
	}

	

}).listen(4000);
/*

	.listen allows us to assign a port to our server. This will allow us serve our index.js server in our localmachine assigned to port 4000. There are several tasks and processes on our computer/machine that run on different port numbers

	hypertext transfer protocol - https://localhost:4000/ - server

	localhost: -- local Machine: 4000- current port assigned to our server
		//listen assigns port numbers
	4000,4040,8000,5000,3000,4200 - usually used for web dev

*/

console.log("Server is running on localHost:4000!");

//terminal - ctrl c, turn off applications in the terminal

/////////////console.log is added to show validation which port number our server is running on. In fact, the more complex our servers become, the more things we can check first before running the server

/*

servers can actually respond differently with different requests.

we start our requests with our URL. A client can start a different request with a different url

https://localhost:4000/ is not the same with http://localhost:4000/profile -

/ = url endpoint (default)
/profile = url endpoint


we can differentiate requests by their endpoints, we should be able to respond differently to different endpoints

information about the url endpoint of the request is in the request object

request.url contains the URL endpoint
/- favicon.ico - browser's default behavior to retrieve the favicon
/profile -
/favicon.ico

different url endpoints, different requests

different requests require different responses

the process or way to respond differently to a request is called a route

*/


